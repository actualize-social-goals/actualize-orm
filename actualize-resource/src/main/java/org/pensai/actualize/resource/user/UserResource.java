package org.pensai.actualize.resource.user;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

// TODO implement base resource with audit field information
@Getter
@Setter
public class UserResource implements Serializable {
    public String username;
    public String password;
    public String email;
    public boolean twoFactorAuth;
}
