package org.pensai.actualize.orm.strategy;

import org.hibernate.cfg.reveng.DelegatingReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.ReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.TableIdentifier;
import org.hibernate.mapping.MetaAttribute;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CustomReverseEngineeringStrategy extends DelegatingReverseEngineeringStrategy {

    private final Set<String> excludedColumns = new HashSet<>();

    private boolean initialized = false;

    public CustomReverseEngineeringStrategy(ReverseEngineeringStrategy delegate) {
        super(delegate);
        initialize();
    }

    private void initialize() {
        if (!initialized) {
            initialized = true;
            excludedColumns.add("created_by");
            excludedColumns.add("created_date");
            excludedColumns.add("updated_by");
            excludedColumns.add("updated_date");
        }
    }

    @Override
    public boolean excludeColumn(TableIdentifier identifier, String columnName) {
        boolean exclude = excludedColumns.contains(columnName.toLowerCase());

        if (exclude) {
            return true;
        }

        return super.excludeColumn(identifier, columnName);
    }

    @Override
    public Map tableToMetaAttributes(TableIdentifier tableIdentifier) {
        Map map = new HashMap<>();

        addMeta(map, "extends", "BaseEntity");
        addMeta(map, "extra-import", "org.pensai.actualize.orm.entity.BaseEntity");
        return map;
    }

    private void addMeta(Map map, String name, String... values) {
        MetaAttribute attr = new MetaAttribute(name);
        for (String value : values) {
            attr.addValue(value);
        }
        map.put(name, attr);
    }
}
