# Actualize-ORM 
Spring Boot / Spring Data project making use of Hibernate to reverse engineer and generate entities for use with the Actualize-Web-Service.

# Prerequisites
- JDK 15
- Maven
- MySQL 8 
- Your favourite Java IDE

# Installation
Clone the repository 
```
git clone TODO URL
cd actualize-orm
```
Log into the MySQL server as root
`mysql -u root -p`

Create a database
`create database actualize;`

Add a user for the project to use to access the database
`create user 'springuser'@localhost' identified by 'springuser'`

Use the provided DDL to create the database schema
```
// Log out
exit

// login as the spring user
mysql -u springuser -p --database="actualize"

// Source the DDL
source <path-to-project-root>/ddl.sql
```
Verify the tables were created
```
show tables;
+---------------------+
| Tables_in_actualize |
+---------------------+
| goal                |
| goal_progress       |
| user                |
| user_profile        |
+---------------------+
```

Update [persistence.xml](https://gitlab.com/actualize-social-goals/actualize-orm/-/blob/master/actualize-persistence/src/main/resources/META-INF/persistence.xml) according to your local MySQL configuration.

(Optional) If you chose your own password for the database user edit the `hibernate.cfg.properties` file found in `actualize-persistence/src/resources` and update the property `hibernate.connection.password`


Build with Maven
`mvn clean install -DskipTests`

Verify entities were successfully generated at `actualize-persistence/target/generated-source/hbm`

If all is well, continue to the [web service setup](https://gitlab.com/Pensai/actualize-web-service)
