create table user
(
    user_id         int unsigned auto_increment
        primary key,
    username        varchar(50)  not null,
    password        varchar(150) not null,
    email           varchar(50)  not null,
    two_factor_auth tinyint(1)   null,
    created_by      varchar(50)  not null,
    created_date    datetime     not null,
    updated_by      varchar(50)  null,
    updated_date    datetime     null
);

create table user_profile
(
    user_profile_id int unsigned auto_increment
        primary key,
    user_id         int unsigned not null,
    created_by      varchar(50)  not null,
    created_date    datetime     not null,
    updated_by      varchar(50)  null,
    updated_date    datetime     null,
    constraint user_profile_ibfk_1
        foreign key (user_id) references user (user_id)
);

create table goal
(
    goal_id         int unsigned auto_increment
        primary key,
    user_profile_id int unsigned null,
    goal_type_cd    int          null,
    constraint goal_ibfk_1
        foreign key (user_profile_id) references user_profile (user_profile_id)
);

create index user_profile_id
    on goal (user_profile_id);

create table goal_progress
(
    goal_progress_id int unsigned auto_increment
        primary key,
    goal_id          int unsigned null,
    isComplete       tinyint(1)   null,
    created_by       varchar(50)  not null,
    created_date     datetime     not null,
    updated_by       varchar(50)  null,
    updated_date     datetime     null,
    constraint goal_progress_ibfk_1
        foreign key (goal_id) references goal (goal_id)
);

create index goal_id
    on goal_progress (goal_id);

create index user_id
    on user_profile (user_id);


