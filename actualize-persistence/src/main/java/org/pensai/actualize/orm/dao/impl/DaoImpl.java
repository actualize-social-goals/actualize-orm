package org.pensai.actualize.orm.dao.impl;

import org.pensai.actualize.orm.dao.GenericDao;
import org.pensai.actualize.orm.entity.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

public class DaoImpl implements GenericDao<BaseEntity, Serializable> {

    static final Logger log = LoggerFactory.getLogger(DaoImpl.class);

    @PersistenceContext
    private final EntityManager entityManager;

    public DaoImpl(EntityManager em /*Todo: context params */) {
        this.entityManager = em;
    }

    @Override
    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    @Override
    public <S extends BaseEntity> S save(S entity) {
        if (entityManager.contains(entity)) {
            entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }
        entityManager.flush();
        return entity;
    }

    @Override
    public <S extends BaseEntity> List<S> findAll(Class<S> clazz, EntityGraph<S> graph) {
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<S> cQuery = cBuilder.createQuery(clazz);
        Root<S> root = cQuery.from(clazz);
        cQuery.select(root);
        TypedQuery<S> query = entityManager.createQuery(cQuery);

        if (graph != null) {
            query.setHint("javax.persistence.loadgraph", graph);

        }

        return query.getResultList();
    }
}
