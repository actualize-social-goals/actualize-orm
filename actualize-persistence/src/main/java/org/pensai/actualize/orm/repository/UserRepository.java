package org.pensai.actualize.orm.repository;

import org.pensai.actualize.orm.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}
