package org.pensai.actualize.orm.audit;

import org.pensai.actualize.orm.entity.BaseEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class AuditListener {

    @PrePersist
    public void createAuditFields(BaseEntity entity) {
        entity.setCreationDate(getTimeNow());
        entity.setCreatedBy(getCurrentUser());
    }

    @PreUpdate
    public void updateAuditFields(BaseEntity entity) {
        entity.setCreationDate(getTimeNow());
        entity.setCreatedBy(getCurrentUser());
    }

    /**
     * Returns a thread bound LocalDateTime instance of the current time.
     * Used to ensure a unique date instance is assigned to each entity
     * for the purpose of auditing.
     *
     * @return A thread bound LocalDateTime instance of the current time
     */
    private LocalDateTime getTimeNow() {
        ThreadLocal<LocalDateTime> tl = new ThreadLocal<>();
        tl.set(LocalDateTime.now());

        return tl.get();
    }

    /**
     * Gets the user associated with the current transaction. Simple for now
     * will likely end up being CallerSystem-SystemUser composition at some point
     *
     * @return
     */
    private String getCurrentUser() {
        return System.getProperty("user.name");
    }
}
