package org.pensai.actualize.orm.dao;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public interface GenericDao<T, ID extends Serializable> {

    public <S extends T> S save(S entity);

    public <S extends T> List<S> findAll(Class<S> clazz, EntityGraph<S> graph);

    public EntityManager getEntityManager();
}